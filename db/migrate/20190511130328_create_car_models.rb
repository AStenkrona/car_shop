class CreateCarModels < ActiveRecord::Migration[5.1]
  def change
    create_table :car_models do |t|
      t.string :brand
      t.string :model
      t.integer :price

      t.timestamps
    end
  end
end
