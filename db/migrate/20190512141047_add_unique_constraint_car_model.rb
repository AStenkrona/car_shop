class AddUniqueConstraintCarModel < ActiveRecord::Migration[5.1]
  def change
    add_index :car_models, [:brand, :model], unique: true
  end
end
