# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users = User.create([{name: "food bar", email: "food@bar.com", password_digest: User.digest("password"), employee_id: 1 }])
employees = Employee.create([{name: "Hjulia Styrén"},
                             {name: "Antonia Cylinder"},
                             {name: "Kalle Bromslöf"},
                             {name: "Johan Sportratt"}])
car_models = CarModel.create([{brand: "BMW", model: "335i", price: 200000},
                              {brand: "Aston Martin", model: "Vanquish", price: 233000},
                              {brand: "Toyota", model: "Prius", price: 150000},
                              {brand: "Volvo", model: "240", price: 100000}])
sales = Sale.create([{employee_id: 2, car_model_id: 3},
                     {employee_id: 4, car_model_id: 2},
                     {employee_id: 4, car_model_id: 4},
                     {employee_id: 1, car_model_id: 1},
                     {employee_id: 3, car_model_id: 1},
                     {employee_id: 3, car_model_id: 1},
                     {employee_id: 2, car_model_id: 2},
                     {employee_id: 2, car_model_id: 3}])