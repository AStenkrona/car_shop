Rails.application.routes.draw do
  root 'static_pages#home'
  get '/home',  to: 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/signup', to: 'users#new'
  get '/employees', to: 'employees#index'
  get '/car_models', to: 'car_models#index'
  
  get '/add_car', to: 'car_models#new'
  post '/add_car', to: 'car_models#create'
  
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  resources :users
  resources :employees
  resources :car_models
  resources :sales
end
