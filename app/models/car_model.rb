class CarModel < ApplicationRecord
  has_many :sales
  validates :price, numericality: { only_integer: true }
  validates :brand, uniqueness: { scope: :model}
end
