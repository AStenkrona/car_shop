class Employee < ApplicationRecord
  has_many :users
  has_many :sales
  
  # Returns the sum that the employee has sold for
  def has_sold_for
    Sale.where(:employee_id => self.id).pluck(:car_model_id).map {|id| CarModel.find(id)}.pluck(:price).sum
  end
end
