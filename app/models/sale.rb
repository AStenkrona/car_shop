class Sale < ApplicationRecord
  belongs_to :employee
  belongs_to :car_model
  # Returns the sum of all sales.
  def Sale.sum_sales
    Sale.pluck(:car_model_id).map {|id| CarModel.find(id)}.pluck(:price).sum
  end
  
end
