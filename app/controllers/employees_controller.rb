class EmployeesController < ApplicationController
  def new
  end
  
  def index
    @employees = Employee.all
    @sales_sum = Sale.sum_sales
  end
  
  def show
      @employee = Employee.find(params[:id])
      @sales = Sale.where(employee_id: params[:id]) 
  end
end
