class CarModelsController < ApplicationController
  def new
    @car_model = CarModel.new
  end
  
  def index
    @car_models = CarModel.all
  end
  
  def destroy
    CarModel.find(params[:id]).destroy
    redirect_back(fallback_location: car_models_path)
  end
  
  def create
    @car_model = CarModel.new(car_model_params)
    if @car_model.save
      flash[:success] = "Added " + @car_model.brand + " " + @car_model.model
      redirect_to car_models_path
    else
      render 'new'
    end
  end
  
  private
  
    def car_model_params
        params.require(:car_model).permit(:brand, :model, :price)
    end
end
