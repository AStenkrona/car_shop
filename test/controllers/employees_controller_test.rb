require 'test_helper'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Car Shop"
  end
  
  test "should get index" do
    get employees_path
    assert_response :success
    assert_select "title", "Employees | #{@base_title}"
  end
end
