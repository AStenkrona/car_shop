require 'test_helper'

class CarModelsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Car Shop"
  end
  
  test "should get index" do
    get car_models_path
    assert_response :success
    assert_select "title", "Car Models | #{@base_title}"
  end
end
