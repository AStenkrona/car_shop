require 'test_helper'

class SaleTest < ActiveSupport::TestCase
  test "test salesSum" do
    sum = Sale.sum_sales
    assert_equal  5 + 13 + 62 + 35, sum
  end
end
