require 'test_helper'

class EmployeeTest < ActiveSupport::TestCase
  def setup
    @employee = employees(:one)
  end
  test "test salesSum" do
    sum = @employee.has_sold_for
    assert_equal  13 + 35, sum
  end
end
