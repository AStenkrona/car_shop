require 'test_helper'

class AddCarModelTest < ActionDispatch::IntegrationTest

 test "invalid existing car_model information" do
    get add_car_path
    assert_no_difference 'CarModel.count' do
    post add_car_path, params: { car_model: { brand:  "CarTech",
                                         model: "230",
                                         price:            123 } }
    end
  end
  
  test "valid car_model information" do
    get add_car_path
    assert_difference 'CarModel.count', 1 do
    post add_car_path, params: { car_model: { brand:  "iomjdfgmiodfg",
                                         model: "230",
                                         price:              343 } }
    end
    follow_redirect!
  end
  
  test "valid existing brand car_model information" do
    get add_car_path
    assert_difference 'CarModel.count', 1 do
    post add_car_path, params: { car_model: { brand:  "CarTech",
                                         model: "2sdf30",
                                         price:              343 } }
    end
    follow_redirect!
  end
end
